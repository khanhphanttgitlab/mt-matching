# MT Matching

## Data
Data explanation (データ説明):
https://ishizue.atlassian.net/wiki/spaces/WGDAIK/pages/1025638567/Daikoku

## Lambda

### Install requirements

```
cd mt_matching
pip install -r requirements.txt
```

### Install AWS CLI, SAM

You need to install [AWS CLI](https://aws.amazon.com/cli/)
and [SAM](https://aws.amazon.com/serverless/sam/).

To configure the AWS CLI, you can refer to tutorials on the Internet.
One way is to edit the file `~/.aws/config` (on Linux).
It might be better to have a profile name (`meetruck` in the example below) to
distinguish between different AWS accounts you have.

```
[profile meetruck]
aws_access_key_id=ABCDEF
aws_secret_access_key=12345
region = ap-northeast-1
```

### Test the server locally

```bash
cd mt_matching
AWS_PROFILE=meetruck FLASK_ENV=development FLASK_APP=app flask run
```

This will behave (almost) the same as the procedure below to test the lambda
locally.
Note that when testing locally, access to S3, for example, will not work if
your account does not have the read permission.

### Test the lambda locally

Start the lambda locally.

```bash
sam build
sam local start-api --profile meetruck
```

### Deploy the lambda

Change the profile in `samconfig.toml` to match your profile and run.

```bash
sam build
sam deploy
```

### Responding time benchmark
We created 4 datasets whose rows are 20, 50, 100, and 500.
For each dataset, 100 requests are sent from local computer to AWS Lambda (server Tokyo) to calculate the responding time.

A request contains two factors in JSON format:
- A dataset (search results)
- User's search

A responding time is a sum of three following factors:
- Time to send a request (contain the data of searching results and user's searching) from outside AWS to Lambda
- Processing time in Lambda (for calculate similarity)
- Time to return the ranking (id of items in order) from Lambda to the destination

RESULTS
```
n = 20, n_req = 100, mean = 787.19 ms, std = 410.15 ms
n = 50, n_req = 100, mean = 821.37 ms, std = 17.58 ms
n = 100, n_req = 100, mean = 1015.82 ms, std = 24.92 ms
n = 500, n_req = 100, mean = 2274.73 ms, std = 70.53 ms
```
- n: number of rows (available vehicles)
- n_req: number of requests
- mean: average time of the requests in millisecond
- std: standard deviation in millisecond
