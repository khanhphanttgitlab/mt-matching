import json
import os
import numpy as np
import requests
from dummy_data import gen_dummy_data


def benchmark(n_data, n_requests):
    api_root = os.getenv('MATCHING_URL', 'http://localhost:5000/')
    rng = np.random.default_rng()
    df = gen_dummy_data(n_data)

    df['unloadingScheduleAt'] = df['unloadingScheduleAt'].astype(str)

    body = {}
    body['items'] = df.to_dict(orient='records')

    req_time = []  # Total request time
    flask_time = []  # Flask handling time
    rank_time = []  # Ranking function execution time
    for i in range(n_requests):
        body['userRequest'] = (
            df.iloc[rng.integers(n_data)].drop('id').to_dict())
        r = requests.post(api_root + '/suggest', json=body)
        assert r.status_code == requests.codes.ok
        resp = r.json()
        req_time.append(r.elapsed.total_seconds() * 1000)
        flask_time.append(resp['benchmark']['req_time'] * 1000)
        rank_time.append(resp['benchmark']['rank_time'] * 1000)

    print(f"n = {n_data}, n_req = {n_requests}, "
          f"total = {np.mean(req_time):.2f} ms, "
          f"flask = {np.mean(flask_time):.2f} ms, "
          f"rank = {np.mean(rank_time):.2f} ms")


def main():
    benchmark(20, 100)
    benchmark(50, 100)
    benchmark(100, 100)
    benchmark(500, 100)


if __name__ == '__main__':
    main()
