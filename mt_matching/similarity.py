import numpy as np
import pandas as pd


class StandardScaler:
    def __init__(self):
        self.mean = 0
        self.std = 1

    def fit(self, x):
        self.mean = np.mean(x)
        self.std = np.std(x)
        if self.std <= 1e-9:
            self.std = 1
        return self

    def transform(self, x):
        return (x - self.mean) / self.std


def dropColumns(dfCalSimilarity):
    dfCalSimilarity = dfCalSimilarity.drop(columns=[
        'id', 'loadingCity_latitude', 'loadingCity_longitude',
        'unloadingCity_latitude', 'unloadingCity_longitude'
    ])
    return dfCalSimilarity


def convertDatetime(df, dfCalSimilarity):
    timestamp = np.array(df.unloadingScheduleAt.view(np.int64) / 10**9)

    # Normalize timestamp
    time_nor = np.reshape(timestamp, (-1, 1))
    scaler = StandardScaler()
    scaler = scaler.fit(time_nor)
    time_nor = scaler.transform(time_nor)

    # Replace by normalized values
    dfCalSimilarity["unloadingScheduleAt"] = time_nor
    return dfCalSimilarity


def calculateHaversineDistance(coordinates, centerCoord):
    #convert degree to redians
    coordinates = np.radians(coordinates)
    centerCoord = np.radians(centerCoord.to_numpy().astype(float))

    #lat+long distance
    dLAT = coordinates.iloc[:, 0] - centerCoord[0]
    dLON = coordinates.iloc[:, 1] - centerCoord[1]

    #Haversine distance
    HavDistances = 6367 * 2 * np.arcsin(
        np.sqrt(
            np.sin(dLAT / 2)**2 + np.cos(np.radians(37.2175900)) *
            np.cos(coordinates.iloc[:, 0]) * np.sin(dLON / 2)**2))

    return HavDistances


def addDistance(df, dfCalSimilarity, search):
    loadingDistance = calculateHaversineDistance(
        df[["loadingCity_latitude", "loadingCity_longitude"]],
        df.iloc[search][["loadingCity_latitude", "loadingCity_longitude"]])
    unloadingDistance = calculateHaversineDistance(
        df[["unloadingCity_latitude", "unloadingCity_longitude"]],
        df.iloc[search][["unloadingCity_latitude", "unloadingCity_longitude"]])
    dfCalSimilarity["loadingDistance"] = loadingDistance
    dfCalSimilarity["unloadingDistance"] = unloadingDistance
    return dfCalSimilarity


def reassignVehicleType(df, dfCalSimilarity, search):
    max_vehicleType = df["vehicleType"].max()
    dfCalSimilarity.loc[
        dfCalSimilarity["vehicleType"] < df.loc[search, "vehicleType"],
        "vehicleType"] = (
            df.loc[search, "vehicleType"] -
            df.loc[df["vehicleType"] < df.loc[search, "vehicleType"],
                   "vehicleType"] + max_vehicleType)
    return dfCalSimilarity


def convert2OneHot(df, dfCalSimilarity):
    one_hot = pd.get_dummies(df.loadingScheduleType,
                             prefix='loadingScheduleType')
    dfCalSimilarity = dfCalSimilarity.drop('loadingScheduleType', axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.loadingProductHandlingType,
                             prefix='loadingProductHandlingType')
    dfCalSimilarity = dfCalSimilarity.drop('loadingProductHandlingType',
                                           axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.unloadingScheduleType,
                             prefix='unloadingScheduleType')
    dfCalSimilarity = dfCalSimilarity.drop('unloadingScheduleType', axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.unloadingProductHandlingType,
                             prefix='unloadingProductHandlingType')
    dfCalSimilarity = dfCalSimilarity.drop('unloadingProductHandlingType',
                                           axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    #one_hot = pd.get_dummies(df.vehicleType, prefix='vehicleType')
    #dfCalSimilarity = dfCalSimilarity.drop('vehicleType',axis = 1)
    #dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.vehicleQuantity, prefix='vehicleQuantity')
    dfCalSimilarity = dfCalSimilarity.drop('vehicleQuantity', axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.vehicleCharacters, prefix='vehicleCharacters')
    dfCalSimilarity = dfCalSimilarity.drop('vehicleCharacters', axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.transportItemCategory,
                             prefix='transportItemCategory')
    dfCalSimilarity = dfCalSimilarity.drop('transportItemCategory', axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    one_hot = pd.get_dummies(df.packageType, prefix='packageType')
    dfCalSimilarity = dfCalSimilarity.drop('packageType', axis=1)
    dfCalSimilarity = dfCalSimilarity.join(one_hot)

    return dfCalSimilarity


def CalculateMahalanobisDistance(df, dfCalSimilarity, WM, search):
    # Convert to numpy
    vehicles_np = dfCalSimilarity.to_numpy()

    # Covariance matrix
    covariance = np.cov(vehicles_np, rowvar=False)

    #  Inverse of the covariance matrix: Covariance matrix power of -1
    #covariance_pm1 = np.linalg.matrix_power(covariance, -1) # real inversed covariance
    covariance_inv = np.linalg.pinv(covariance)  #pseudo inversed covariance

    # Distances between a selected observation and other observations
    distances_nor = []  # default weights
    distances_WM = []  # selected weights

    # Choose row "#search" as the selected observation
    obs = vehicles_np[search, :]

    # Calculate distances
    for i, val in enumerate(vehicles_np):
        distance_nor = (val - obs).T.dot(covariance_inv).dot(val - obs)
        distances_nor.append(distance_nor)

        distance_WM = (val - obs).T.dot(
            (WM).dot(covariance_inv).dot(WM)).dot(val - obs)
        distances_WM.append(distance_WM)

    distances_nor = np.array(distances_nor)
    distances_WM = np.array(distances_WM)

    # Add calculated distances into dataframe
    df['MD_nor'] = distances_nor
    df['MD_WM'] = distances_WM

    df.sort_values("MD_WM", inplace=True)

    return list(df.id)


def main(df, request):
    #df = pd.read_csv('vehicles.csv')
    search = request
    df.sort_index(inplace=True)
    dfCalSimilarity = df.copy()

    # Preprocess
    dfCalSimilarity = dropColumns(
        dfCalSimilarity)  #this dataframe is used for calculate similarity
    dfCalSimilarity = convertDatetime(df, dfCalSimilarity)

    dfCalSimilarity = addDistance(df, dfCalSimilarity, search)
    dfCalSimilarity = reassignVehicleType(df, dfCalSimilarity, search)
    dfCalSimilarity = convert2OneHot(df, dfCalSimilarity)

    # Set feature weights
    # Default weight = 1 for each feature
    size = dfCalSimilarity.shape
    W = np.ones((size[1]), int)

    # Define new weight for each feature
    W[0] = 3  # unloadingScheduleAt
    W[1] = 3  # desiredPrice
    W[8] = 3  # loadingDistance
    W[9] = 3  # unloadingDistance

    # Weighted Matrix (WM)
    WM = np.zeros((size[1], size[1]), int)
    np.fill_diagonal(WM, W)

    #Calculate Mahalanobis Distance
    idx = CalculateMahalanobisDistance(df, dfCalSimilarity, WM, search)

    return idx
