import awsgi
import boto3
import marshmallow.exceptions
import numpy as np
import pandas as pd
from flask import Flask
from flask import request
from marshmallow import Schema
from marshmallow import fields
import pandas as pd
import similarity
import time

app = Flask(__name__)


class ItemSchema(Schema):
    id = fields.Int(required=True)
    loadingCity_latitude = fields.Float(required=True)
    loadingCity_longitude = fields.Float(required=True)
    loadingScheduleType = fields.Int(required=True)
    loadingProductHandlingType = fields.Int(required=True)
    unloadingScheduleType = fields.Int(required=True)
    unloadingScheduleAt = fields.DateTime(required=True)
    unloadingCity_latitude = fields.Float(required=True)
    unloadingCity_longitude = fields.Float(required=True)
    unloadingProductHandlingType = fields.Int(required=True)
    vehicleType = fields.Int(required=True)
    vehicleQuantity = fields.Int(required=True)
    vehicleCharacters = fields.Int(required=True)
    desiredPrice = fields.Int(required=True)
    transportItemCategory = fields.Int(required=True)
    isStackable = fields.Int(required=True)
    packageType = fields.Int(required=True)
    parcelAmount = fields.Int(required=True)
    weight = fields.Int(required=True)
    volume = fields.Int(required=True)


class MatchingRequestSchema(Schema):
    items = fields.List(fields.Nested(ItemSchema()), required=True)
    userRequest = fields.Nested(ItemSchema(exclude=('id', )), required=True)


@app.route('/welcome')
def welcome():
    return {
        'message': 'Welcome! This is MeeTruck Matching.',
    }


@app.route('/feature_test')
def feature_test():
    s3 = boto3.client('s3')
    resp = s3.get_object(Bucket='mt-matching', Key='locationMasterInJapan.csv')
    df = pd.read_csv(resp['Body'])
    s = pd.Series([1, 3, 5, np.nan, 6, 8])
    a = np.arange(1, 101, dtype=int)
    return {
        'message': 'Welcome! This is MeeTruck Matching.',
        'pandas': s.mean(),
        'numpy': int(np.sum(a)),
        's3': len(df),
    }


@app.route('/suggest', methods=['POST'])
def suggest():
    req_start = time.time()
    match_req = MatchingRequestSchema().load(request.json)
    df = pd.DataFrame(match_req['items'])
    user_req = pd.Series(match_req['userRequest'] | {'id': -1})
    df = df.append(user_req, ignore_index=True)
    rank_start = time.time()
    rank = similarity.main(df, len(df) - 1)
    rank_end = time.time()
    rank.remove(-1)
    req_end = time.time()
    return {
        'status': 0,
        'data': rank,
        'benchmark': {
            'req_time': req_end - req_start,
            'rank_time': rank_end - rank_start,
        }
    }


@app.errorhandler(marshmallow.exceptions.ValidationError)
def bad_input(e):
    return {'status': 1, 'message': str(e)}, 400


def lambda_handler(event, context):
    return awsgi.response(app, event, context)
