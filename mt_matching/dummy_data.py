import numpy as np
import pandas as pd
import datetime


def gen_dummy_data(n):
    loadingScheduleType = [0, 1]
    loadingProductHandlingType = [0, 1]
    unloadingScheduleType = [0, 1]
    data = []

    rng = np.random.default_rng()
    for i in range(n):
        data.append([
            i, # id
            rng.uniform(low=20, high=45),  #1 loadingCity_latitude
            rng.uniform(low=122, high=153),  #2 loadingCity_longitude
            rng.choice(loadingScheduleType),  #3 loadingScheduleType
            rng.choice(
                loadingProductHandlingType),  #4 loadingProductHandlingType
            rng.choice(unloadingScheduleType),  #5 unloadingScheduleType
            datetime.datetime(2021, 10, 1) +
            datetime.timedelta(seconds=rng.random() *
                               (31 * 86400)),  #6 unloadingScheduleAt
            rng.uniform(low=20, high=45),  #9 unloadingCity_latitude
            rng.uniform(low=122, high=153),  #10 unloadingCity_longitude
            rng.integers(5) + 1,  #11 unloadingProductHandlingType
            rng.integers(16) + 1,  #12 vehicleType
            rng.integers(10) + 1,  #13 vehicleQuantity
            rng.integers(20) + 1,  #14 vehicleCharacters
            rng.integers(100) + 1,  #15 desiredPrice
            rng.integers(7) + 1,  #16 transportItemCategory
            rng.integers(2),  #17 isStackable
            rng.integers(7) + 1,  #18 packageType
            rng.integers(20) + 1,  #19 parcelAmount
            rng.integers(20) + 1,  #20 weight
            rng.integers(100) + 1,  #21 volume
        ])

    vehicles = pd.DataFrame(
        data,
        columns=[
            'id', 'loadingCity_latitude', 'loadingCity_longitude',
            'loadingScheduleType', 'loadingProductHandlingType',
            'unloadingScheduleType', 'unloadingScheduleAt',
            'unloadingCity_latitude', 'unloadingCity_longitude',
            'unloadingProductHandlingType', 'vehicleType', 'vehicleQuantity',
            'vehicleCharacters', 'desiredPrice', 'transportItemCategory',
            'isStackable', 'packageType', 'parcelAmount', 'weight', 'volume'
        ])
    return vehicles
